/* global Backendless, Handlebars */

$(function () {
     var APPLICATION_ID = "06DC0A54-D4DE-5E72-FF16-7585DBE53A00",
         SECRET_KEY = "72AD4FDE-44A7-C2AB-FF78-E89330A85300",
         VERSION= "v1";
         
     Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
    
     var postsCollection = Backendless.Persistence.of(Posts).find();
     
     console.log(postsCollection);
     
     var wrapper ={
         posts: postsCollection.data
     };
     
     Handlebars.registerHelper('format', function (time) {
         return moment(time).format("dddd,MMMM Do YYYY");
     });
     
     var blogScript = $("#blogs-template").html();
     var blogTemplate = Handlebars.compile(blogScript);
     var blogHTML = blogTemplate(wrapper);
     
     $('.main-container').html(blogHTML);
 
 });
 
 function Posts(args){
     args = args || {};
     this.title = args.title || "";
     this.content = args.content || "";
     this.authorEmail = args.authorEmail || "";
     
 } 